﻿using UnityEngine;
using System.Collections;

public class SinkFloor : MonoBehaviour {

	public float sinkSpeed = 10;
	public bool userTouch = false;
	public float minYPos;
	Vector3 initialPosition;
	public bool isSinking = false;
	float initialTimeDelay = 0;
	public bool initBox;

	// Use this for initialization
	void Start () {
		//userTouch = true;
		//isSinking = true;
		initialPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (initBox)
			return;
		if (userTouch && !isSinking) {
			isSinking = true;
		}

		if (isSinking) {
			if (transform.position.y > minYPos) {
				transform.Translate (Vector3.down * sinkSpeed * Time.deltaTime);
				isSinking = false;
			}
		}
	}

	void OnCollisionEnter(Collision colision){
		userTouch = true;
	}

	public void reset(){
		userTouch = false;
		isSinking = false;
		transform.position = initialPosition;
	}
}
