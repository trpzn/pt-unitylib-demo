﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public GameObject player;
	public float outBoundAltitude = -6;

	public GameObject floor;

	Vector3 startPosition;
	// Use this for initialization
	void Start () {
		startPosition = player.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position.y <= outBoundAltitude) {
			player.transform.position = startPosition;
			player.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			resetStage ();
		}
	}

	void resetStage(){
		foreach (Transform child in floor.transform) {
			SinkFloor currentfloor = child.gameObject.GetComponent<SinkFloor> ();
			if (currentfloor != null) {
				currentfloor.reset ();
			}
			//schild.gameObject.GetComponent<SinkFloor> ()
		}
	}


}
