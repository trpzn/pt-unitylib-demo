﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BpmManager : MonoBehaviour {
	public Asarduinity_bpm asarduinity_bpm;
	public UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController firstPersonController;
	public Vector2 bpmRange;
	public Vector2 speed;

	public float currentBpm = 0f;
	public float currentSpeed = 1f;

	public Text bpmText;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		updateBpm ();

		if (currentBpm >= bpmRange.x || currentBpm <= bpmRange.y) {
			if (currentBpm < 50) {
				currentSpeed = 0.1f;
			}else if(currentBpm < 80){
				currentSpeed = 1f;
			}else if(currentBpm < 100){
				currentSpeed = 1.2f;
			}else if(currentBpm < 110){
				currentSpeed = 1.5f;
			}
		} else {
			currentSpeed = 0f;
		}

		bpmText.text = currentBpm.ToString();

		updateCurrentSpeed ();
	}

	void updateBpm(){
		currentBpm = asarduinity_bpm.bpm;
	}

	void updateCurrentSpeed(){
		firstPersonController.movementSettings.SpeedMultiplier = currentSpeed;
	}
}
