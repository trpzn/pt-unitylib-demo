﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using UnityEngine.UI;
using System.Threading;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System;

public class SerialManager{
	public string serialportWindows;
	public string serialportLinux;
	string inputBuffer;
	bool conected = false;
	float internalClock = 0;
	public float readEveryXMilisecs = 10;
	SerialPort mySerial;
	bool readRunning = true;
	public string ip = "127.0.0.1";


	private Queue writeBuffer = new Queue ();
	private Queue readBuffer = new Queue ();
	private ReadSerial readSerial;
	private Thread readSerialWorker;


	internal bool socketReady = false;
	UdpClient udpClient;
	TcpClient mySocket;
	NetworkStream theStream;
	StreamWriter theWriter;
	StreamReader theReader;
	string Host = "127.0.0.1";
	int Port = 9999;

	ASardunity asarduinity;

	public SerialManager(ASardunity asarduinity,string com){
		serialportWindows = com;
		this.asarduinity = asarduinity;
		conectar ();
	}

	public void setHost(string newHost){
		Host = newHost;
	}

	// Return the first message FIFO
	public int read(){
		if (readBuffer.Count > 0) {
			return (int)readBuffer.Dequeue ();
		} else {
			return -1;
		}
	}
	public void conectar(){
		Debug.Log ("Conectar");
		WaitForHost waiter = new WaitForHost(this);
		Thread waiterThread = new Thread(waiter.DoWork);
		waiterThread.Start();
	}

	public void conectarfinally(){
		Debug.Log (Host);
		if (conected)
			return;
		//mySerial = new SerialPort ();
		//Debug.Log (serialportWindows);
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
		//mySerial.PortName = serialportWindows;
		#elif UNITY_EDITOR_LINUX || UNITY_STANDALONE_LINUX
		mySerial.PortName = serialportLinux;
		#endif

		//Debug.Log (mySerial.PortName);
		//mySerial.BaudRate = 256000;
		//mySerial.ReadTimeout = 1;
		//mySerial.ReadBufferSize = 1024;
		//mySerial.Open ();
		//try
		//{

		mySocket = new TcpClient(Host, Port);
		theStream = mySocket.GetStream();
		theWriter = new StreamWriter(theStream);
		theReader = new StreamReader(theStream);
		socketReady = true;
		conected = true;

		//mySerial.DiscardInBuffer();
		//Debug.Log("Serial Port {0} Opened" + mySerial.PortName);
		readSerial = new ReadSerial(theReader,readBuffer);
		readSerialWorker = new Thread(readSerial.DoWork);
		readSerialWorker.Start();

		//StartCoroutine("ReadFromSerial");
		//text.GetComponent<Text>()
		//text.text = "Serial Port {0} Opened" + mySerial.PortName;

		//}
		/*
		catch
		{
			//text.text = ;
			Debug.Log("ERROR in Opening Serial Port \n Corra como root");
		}
		*/
	}

	void OnApplicationQuit() {
		//readRunning = false;
		//mySerial.Write ("0");
		theWriter.Close();
		theReader.Close();
		mySocket.Close();
		socketReady = false;
		//mySerial.Close ();
	}
	void readData(){
		if (conected) {
			if (internalClock < (readEveryXMilisecs/1000)) {
				internalClock += Time.deltaTime;
				return;
			}
			//Debug.Log (1);
			//Debug.Log (internalClock);
			internalClock = 0;

		}
	}

	public void write(byte data){
		Debug.Log (data);
		byte[] x = { data };
		try{
			theWriter.Write(x);
			theWriter.Flush();
			//mySerial.Write (x,0,1);
		}catch{

		}
		//mySerial.write
	}
	void dataReceived(){
		//Debug.Log (inputBuffer);
		//gameManager.dataReceivedFromSerial (inputBuffer);
		inputBuffer = "";
	}
	public void readEnqueue(int data){
		readBuffer.Enqueue (data);
	}

	public static IEnumerator WaitForRealSeconds(float time){
		float start = Time.realtimeSinceStartup;
		while(Time.realtimeSinceStartup<start + time){
			yield return null;
		}
	}

	public class ReadSerial{
		private volatile bool _shouldStop;
		//SerialManager serial;
		StreamReader mySerial;
		Queue readBuffer;
		public ReadSerial(StreamReader mySerial,Queue readBuffer){
			this.mySerial = mySerial;
			this.readBuffer = readBuffer;
		}
		public void DoWork()
		{
			while(!_shouldStop){
				try{
					int x = mySerial.Read();
					if (x != 65533){
						//Debug.Log(x);
						readBuffer.Enqueue(x);
					}

				}catch{
					//Debug.Log ("error");
				}
			}
		}

		public void RequestStop()
		{
			_shouldStop = true;
		}

	}

	public void Stop(){
		readSerial.RequestStop();
		readSerialWorker.Join ();
		mySerial.Close ();
		Debug.Log ("WellClosed");
	}


	public class WaitForHost
	{
		string status = "searchingserver";
		private const int listenPort = 9997;
		string Host;
		bool done = false;
		UdpClient listener;
		IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);
		string received_data;
		byte[] receive_byte_array;
		SerialManager serialMan;
		public WaitForHost(SerialManager serialMan){
			this.serialMan = serialMan;
			try{
				listener = new UdpClient(listenPort);
			}
			catch(Exception e){
				Debug.Log(e.ToString());
			}
		}
		public void DoWork(){
			try
			{
				while (!done)
				{		
					Debug.Log(received_data);
					Thread.Sleep(2);
					// this is the line of code that receives the broadcase message.
					// It calls the receive function from the object listener (class UdpClient)
					// It passes to listener the end point groupEP.
					// It puts the data from the broadcast message into the byte array
					// named received_byte_array.
					// I don't know why this uses the class UdpClient and IPEndPoint like this.
					// Contrast this with the talker code. It does not pass by reference.
					// Note that this is a synchronous or blocking call.
					receive_byte_array = listener.Receive(ref groupEP);
					Host = groupEP.ToString();
					//Console.WriteLine("Received a broadcast from {0}", groupEP.ToString() );
					received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
					//Console.WriteLine("data follows \n{0}\n\n", received_data);


					if (status == "searchingserver"){						
						Debug.Log(received_data);
						string data  = received_data;
						if (data == "KRAKENBROADCAST"){
							Debug.Log("server found");
							status = "connected";
							serialMan.setHost(Host.Split(':')[0]);
							serialMan.conectarfinally();
						}
					}
					if(status == "connected"){
						done = true;							
					}

				}
			}
			catch (Exception e)
			{
				Debug.Log(e.ToString());
			}
		}
	} // end of class UDPListener
}

/*
public class SerialManager{
	public string serialportWindows;
	public string serialportLinux;
	string inputBuffer;
	bool conected = false;
	float internalClock = 0;
	public float readEveryXMilisecs = 10;
	SerialPort mySerial;
	bool readRunning = true;
	//public string ip;

	private Queue writeBuffer = new Queue ();
	private Queue readBuffer = new Queue ();
	private ReadSerial readSerial;
	private Thread readSerialWorker;

	ASardunity asarduinity;

	public SerialManager(ASardunity asarduinity,string com){
		serialportWindows = com;
		this.asarduinity = asarduinity;
		conectar ();
	}

	public int read(){
		if (readBuffer.Count > 0) {
			return (int)readBuffer.Dequeue ();
		} else {
			return -1;
		}
	}

	public void conectar(){
		if (conected)
			return;
		mySerial = new SerialPort ();
		//Debug.Log (serialportWindows);
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
		mySerial.PortName = serialportWindows;
		#elif UNITY_EDITOR_LINUX || UNITY_STANDALONE_LINUX
		mySerial.PortName = serialportLinux;
		#endif

		Debug.Log (mySerial.PortName);
		mySerial.BaudRate = 256000;
		mySerial.ReadTimeout = 1;
		mySerial.ReadBufferSize = 1024;
		//mySerial.Open ();
		try
		{

			mySerial.Open();//Open the Port
			conected = true;
			mySerial.DiscardInBuffer();
			Debug.Log("Serial Port {0} Opened" + mySerial.PortName);
			readSerial = new ReadSerial(mySerial,readBuffer);
			readSerialWorker = new Thread(readSerial.DoWork);
			readSerialWorker.Start();

			//StartCoroutine("ReadFromSerial");


			//text.GetComponent<Text>()
			//text.text = "Serial Port {0} Opened" + mySerial.PortName;

		}
		catch
		{
			//text.text = ;
			Debug.Log("ERROR in Opening Serial Port \n Corra como root");
		}
	}


	void OnApplicationQuit() {
		//readRunning = false;
		//mySerial.Write ("0");
		mySerial.Close ();
	}
	void readData(){
		if (conected) {
			if (internalClock < (readEveryXMilisecs/1000)) {
				internalClock += Time.deltaTime;
				return;
			}
			//Debug.Log (1);
			//Debug.Log (internalClock);
			internalClock = 0;

		}
	}

	public void write(byte data){
		Debug.Log (data);
		byte[] x = { data };
		try{
			mySerial.Write (x,0,1);
		}catch{

		}
		//mySerial.write
	}
	void dataReceived(){
		//Debug.Log (inputBuffer);
		//gameManager.dataReceivedFromSerial (inputBuffer);
		inputBuffer = "";
	}
	public void readEnqueue(int data){
		readBuffer.Enqueue (data);
	}

	public static IEnumerator WaitForRealSeconds(float time){
		float start = Time.realtimeSinceStartup;
		while(Time.realtimeSinceStartup<start + time){
			yield return null;
		}
	}

	public class ReadSerial{
		private volatile bool _shouldStop;
		//SerialManager serial;
		SerialPort mySerial;
		Queue readBuffer;
		public ReadSerial(SerialPort mySerial,Queue readBuffer){
			this.mySerial = mySerial;
			this.readBuffer = readBuffer;
		}
		public void DoWork()
		{
			while(!_shouldStop){
				try{
					int x = mySerial.ReadByte();
					//Debug.Log(x);
					readBuffer.Enqueue(x);

				}catch{
					//Debug.Log ("error");
				}
			}
		}

		public void RequestStop()
		{
			_shouldStop = true;
		}

	}

	public void Stop(){
		readSerial.RequestStop();
		readSerialWorker.Join ();
		mySerial.Close ();
		Debug.Log ("WellClosed");
	}

}
*/
