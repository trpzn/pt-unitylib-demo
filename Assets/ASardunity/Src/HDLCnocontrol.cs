﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class HDLCnocontrol : Crc.CRCITT{
	uint FRAME_BOUNDARY_OCTET = 0x7E;
	uint CONTROL_ESCAPE_OCTET = 0x7d;
	uint INVERT_OCTET = 0x20;
	uint CRC16_CCITT_INIT_VAL = 0xFFFF;
	uint CRC_LENGTH = 2;
	uint FRAME_BOUNDARY_LENGTH = 1;
	uint ADDRESS_LENGTH = 1;
	uint MIN_REQ_DATA = 4;
	uint MAX_FRAME_LENGTH = 32;

	int framePosition = 0;
	List<uint> receiveFrameBuffer = new List<uint>();
	bool escapeCharacter = false;
	uint frameChecksum;

	ASardunity comRouter;
	bool noCrop;

	public HDLCnocontrol(ASardunity comRouter,bool noCrop){
		this.comRouter = comRouter;
		this.noCrop = noCrop;
		this.frameChecksum = CRC16_CCITT_INIT_VAL;
	}

	public void charReceiver(uint data){
		data = uint_8(data);

		if (data == FRAME_BOUNDARY_OCTET) {
			if(escapeCharacter)
			{
				escapeCharacter = false;
			}
			else if(
				receiveFrameBuffer.Count >=2 && (frameChecksum == (
					receiveFrameBuffer[receiveFrameBuffer.Count -1] << 8 |
					receiveFrameBuffer[receiveFrameBuffer.Count -2] & 0xFF
				))
			)
			{
				if(receiveFrameBuffer.Count > MIN_REQ_DATA){
					uint address = receiveFrameBuffer [0];
					//Debug.Log (frameToSting(receiveFrameBuffer.ToArray()));
					if (noCrop) {
						List<uint> temp = new List<uint> ();
						temp.Add (FRAME_BOUNDARY_OCTET);
						temp.AddRange (receiveFrameBuffer);
						temp.Add (FRAME_BOUNDARY_OCTET);
						receiveFrameBuffer = temp;
					} else {
						receiveFrameBuffer.RemoveRange (receiveFrameBuffer.Count-2,2);
					}
					//Debug.Log (frameToSting(receiveFrameBuffer.ToArray()));
					//List<uint> tempx = receiveFrameBuffer;
					comRouter.frameHandler (receiveFrameBuffer.ToArray(),address);
					receiveFrameBuffer = new List<uint>();
				}else{
					clearBuffer();
				}
			}
			framePosition = 0;
			frameChecksum = CRC16_CCITT_INIT_VAL;
			return;
		}
		if(escapeCharacter)
		{
			escapeCharacter = false;
			data ^= INVERT_OCTET;
		}
		else if(data == CONTROL_ESCAPE_OCTET)
		{
			escapeCharacter = true;
			return;
		}

		receiveFrameBuffer.Add(data);
		if(receiveFrameBuffer.Count - 3 >= 0)
		{
			frameChecksum = crcUpdate(
				frameChecksum,
				receiveFrameBuffer[receiveFrameBuffer.Count-3]
			);
		}

		if(receiveFrameBuffer.Count == MAX_FRAME_LENGTH)
		{
			receiveFrameBuffer.Clear();
			frameChecksum = CRC16_CCITT_INIT_VAL;
		}
	}

	public void frameDecode(uint[] frame,uint address){
		//uint receiver_address = frame [0];
		uint data;
		uint fcs = CRC16_CCITT_INIT_VAL;

		comRouter.sendCharFunction (parseByte(FRAME_BOUNDARY_OCTET));
		fcs = crcUpdate(
			fcs,
			address
		);
		/*
		 * falta scape octet
		 * 
		 */
		comRouter.sendCharFunction (parseByte(address));
		//int time = -1;
		foreach(uint currData in frame){
			data = currData;
			fcs = crcUpdate(
				fcs,
				data
			);
			if((data == CONTROL_ESCAPE_OCTET) || (data == FRAME_BOUNDARY_OCTET))
			{
				comRouter.sendCharFunction(parseByte(CONTROL_ESCAPE_OCTET));
				data ^= INVERT_OCTET;
			}
			comRouter.sendCharFunction(parseByte(data));
		}
		data = low8 (fcs);
		if((data == CONTROL_ESCAPE_OCTET) || (data == FRAME_BOUNDARY_OCTET))
		{
			comRouter.sendCharFunction(parseByte(CONTROL_ESCAPE_OCTET));
			data ^= INVERT_OCTET;
		}
		comRouter.sendCharFunction(parseByte(data));
		data = high8 (fcs);
		if((data == CONTROL_ESCAPE_OCTET) || (data == FRAME_BOUNDARY_OCTET))
		{
			comRouter.sendCharFunction(parseByte(CONTROL_ESCAPE_OCTET));
			data ^= INVERT_OCTET;
		}
		comRouter.sendCharFunction(parseByte(data));
		comRouter.sendCharFunction(parseByte(FRAME_BOUNDARY_OCTET));
	}
	
	private byte parseByte(uint x){
		return byte.Parse (x.ToString());
	}

	private void charSender(byte byteToSend){
		comRouter.sendCharFunction (byteToSend);
	}

	private void clearBuffer(){
		receiveFrameBuffer.Clear();
	}

	string frameToSting(uint[] frame){
		string output = "";
		if (frame.Length > 0) {
			output += "[";
		} else {
			return output;
		}
		foreach (uint data in frame) {
			output += data + ",";
		}
		output += "]";
		return output;
	}

}
