﻿using System.Collections;

namespace Crc{
	
	public class CRCITT
	{
		const uint mask8 = (1 << 8) - 1;
		const uint mask16 = (1 << 16) - 1;
		public uint uint_8(uint x){
			return x & mask8;
		}

		public uint uint_16(uint x){
			return x & mask16;
		}

		public uint crcUpdate(uint crc, uint data){
			crc = uint_16 (crc);
			data = uint_8 (data);
			data ^= low8 (crc);
			data ^= uint_8 (data << 4);
			return uint_16 (
				(uint_16 (data) << 8 | high8 (crc)) ^
				uint_8 (data >> 4) ^
				(uint_16 (data) << 3)
			);
		}
		public uint low8(uint crc){
			crc = uint_16 (crc);
			return uint_8 (crc & 0xff);
		}
		public uint high8(uint crc){
			crc = uint_16 (crc);
			return uint_8 ((crc >> 8) & 0xff);
		}
	}

}

